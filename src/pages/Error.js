import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {Link} from 'react-router-dom';



export default function Error() {
return (
    <Row>
    	<Col className="p-5">
            <h1>Page Not Found</h1>
            <p>Go back to the <Link to ="/">Homepage.</Link></p>
        </Col>
    </Row>
	)
}