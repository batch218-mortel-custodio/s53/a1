
import {useState, useEffect} from 'react';
import { Form, Button } from 'react-bootstrap';
import {useNavigate} from 'react-router-dom';

export default function Login(props) {

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [isActive, setIsActive] = useState(false);


	const navigate = useNavigate();

	function registerUser(e){
		e.preventDefault()

		localStorage.setItem('email', email);

		setEmail("");
		setPassword("");
		navigate('/');
		
		alert("Login successful!");
	}


	useEffect(() => {
		if (email !== '' && password !== ''){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}

	}, [email, password])

    return (
        <Form onSubmit={(e) => registerUser(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email" value={email} 
	                onChange={e => setEmail(e.target.value)}
	                required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" value={password}
	                placeholder="Password"
	                onChange={e => setPassword(e.target.value)} 
	                required
                />
            </Form.Group>

            	{/*// ternary operator*/}
            	{ isActive ? 

            	<Button variant="primary" type="submit" id="submitBtnLogin">
            		Login
            	</Button>
            	:
            	<Button variant="danger" type="submit" id="submitBtnLogin" disabled>
            		Login
            	</Button>     	
            	}


        </Form>
    )

}