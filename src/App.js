import './App.css';

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import AppNavbar from './components/AppNavbar.js';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';


import {Container} from 'react-bootstrap';

// Single Page Application (like facebook)
function App() {
  return (
    <>
    <Router>
      <AppNavbar />
      <Container>
        <Routes>
            <Route path="*" element={<Error />}/>
            <Route path="/" element={<Home />} />
            <Route path="/courses" element={<Courses />} />
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
          </Routes>
      </Container>
    </Router>
    </>
  );
}

export default App;
